let cubeIt = 7;
let getCube = Math.pow(cubeIt,3);
console.log(`The cube of ${cubeIt} is ${getCube}`);

const address = [2811,"Begonia St.","Brgy. Fatima","San Pedro City","Laguna",4023];
let [houseNumber,street,barangay,municipality,Province,zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${barangay}, ${municipality}, ${Province} ${zipCode}`);

let animal = {name:"Tokyo",species:"Azkals",weight:"10 kg",measurement:"3 ft 6 in"};
let {name,species,weight,measurement} = animal;
console.log(`${name} is a/an ${species}. He weighed at ${weight} with a measurement of  ${measurement}.`);

let numbers = [1,2,3,4,5];

/*		numbers.forEach(
			function(numbers){

			console.log(numbers)
			}
		)*/

numbers.forEach((numbers) => console.log(numbers));


/*let reduceNumber = numbers.reduce(
			function(c,n) {
				return c+n;
			}
		)

console.log(reduceNumber);*/


console.log(numbers.reduce((c,n) => c+n));




class Dog {
	constructor(dogName,dogAge,dogBreed) {
		this.name = dogName;
		this.age = dogAge;
		this.breed = dogBreed;
	}
}

let dog = new Dog("Jackie", 5, "Garuda Eagle");
console.log(dog);